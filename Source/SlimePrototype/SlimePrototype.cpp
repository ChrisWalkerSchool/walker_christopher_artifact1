// Copyright Epic Games, Inc. All Rights Reserved.

#include "SlimePrototype.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SlimePrototype, "SlimePrototype" );
