// Copyright Epic Games, Inc. All Rights Reserved.

#include "SlimePrototypeGameMode.h"
#include "SlimePrototypeCharacter.h"

ASlimePrototypeGameMode::ASlimePrototypeGameMode()
{
	// Set default pawn class to our character
	DefaultPawnClass = ASlimePrototypeCharacter::StaticClass();	
}
